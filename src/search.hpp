
#include <string>
#include <iostream>

using namespace std;


int search(auto text, auto pattern){
	
	for(unsigned int i = 0; i < text.size(); i++){
		
		bool w = true; 
		
		if(pattern.size() == 0){
			return -1;
		}
		
		for(unsigned int h = 0; h < pattern.size(); h++){
			
			if(text[i+h] != pattern[h]){
				w = false;
				break;
			}
		}
		
		if(w == true){
			return i;
		}
	}
	
	return -1;
}//end search

		
	
